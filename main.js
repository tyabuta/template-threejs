class App {

    init(){
        this.renderer = new THREE.WebGLRenderer();
        this.renderer.setSize( 800, 600 );
        document.body.appendChild( this.renderer.domElement );

        this.scene = new THREE.Scene();

        this.camera = new THREE.PerspectiveCamera(
            35,				// Field of view
            800 / 600,		// Aspect ratio
            0.1,			// Near plane
            10000			// Far plane
        );
        this.camera.position.set( -15, 10, 10 );
        this.camera.lookAt( this.scene.position );

        let geometry = new THREE.BoxGeometry( 5, 5, 5 );
        let material = new THREE.MeshLambertMaterial( { color: 0xFF0000 } );
        this.mesh = new THREE.Mesh( geometry, material );
        this.scene.add( this.mesh );

        let light = new THREE.PointLight( 0xFFFF00 );
        light.position.set( 10, 0, 10 );
        this.scene.add( light );

        this.render();
    }

    update(){
        this.mesh.rotation.x += 0.01;
    }

    render() {
        requestAnimationFrame(() => this.render());
        this.renderer.setClearColor( 0xdddddd, 1);
        this.update();
        this.renderer.render(this.scene, this.camera);
    }
}

window.onload = () => {
    let app = new App();
    app.init();
};


